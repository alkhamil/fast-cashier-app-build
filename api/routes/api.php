<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\CustomerController;
use App\Http\Controllers\API\ExpenseController;
use App\Http\Controllers\API\JournalController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\PurchaseOrderController;
use App\Http\Controllers\API\SalesOrderController;
use App\Http\Controllers\API\SupplierController;
use App\Http\Controllers\API\UnitController;
use App\Http\Controllers\API\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    // Begin User
    Route::get('user', [UserController::class, 'index']);
    Route::get('user/view', [UserController::class, 'view']);
    Route::post('user/save', [UserController::class, 'save']);
    Route::post('user/update', [UserController::class, 'update']);
    Route::get('user/delete', [UserController::class, 'delete']);
    Route::get('user/restore', [UserController::class, 'restore']);
    // End User

    // Begin Supplier
    Route::get('supplier', [SupplierController::class, 'index']);
    Route::get('supplier/view', [SupplierController::class, 'view']);
    Route::post('supplier/save', [SupplierController::class, 'save']);
    Route::post('supplier/update', [SupplierController::class, 'update']);
    Route::get('supplier/delete', [SupplierController::class, 'delete']);
    Route::get('supplier/restore', [SupplierController::class, 'restore']);
    // End Supplier

    // Begin Customer
    Route::get('customer', [CustomerController::class, 'index']);
    Route::get('customer/view', [CustomerController::class, 'view']);
    Route::post('customer/save', [CustomerController::class, 'save']);
    Route::post('customer/update', [CustomerController::class, 'update']);
    Route::get('customer/delete', [CustomerController::class, 'delete']);
    Route::get('customer/restore', [CustomerController::class, 'restore']);
    // End Customer

    // Begin Category
    Route::get('category', [CategoryController::class, 'index']);
    Route::get('category/view', [CategoryController::class, 'view']);
    Route::post('category/save', [CategoryController::class, 'save']);
    Route::post('category/update', [CategoryController::class, 'update']);
    Route::get('category/delete', [CategoryController::class, 'delete']);
    Route::get('category/restore', [CategoryController::class, 'restore']);
    // End Category

    // Begin Unit
    Route::get('unit', [UnitController::class, 'index']);
    Route::get('unit/view', [UnitController::class, 'view']);
    Route::post('unit/save', [UnitController::class, 'save']);
    Route::post('unit/update', [UnitController::class, 'update']);
    Route::get('unit/delete', [UnitController::class, 'delete']);
    Route::get('unit/restore', [UnitController::class, 'restore']);
    // End Unit

    // Begin Product
    Route::get('product', [ProductController::class, 'index']);
    Route::get('product/view', [ProductController::class, 'view']);
    Route::post('product/save', [ProductController::class, 'save']);
    Route::post('product/update', [ProductController::class, 'update']);
    Route::get('product/delete', [ProductController::class, 'delete']);
    Route::get('product/restore', [ProductController::class, 'restore']);
    Route::get('product/list-category', [ProductController::class, 'listCategory']);
    Route::get('product/list-unit', [ProductController::class, 'listUnit']);
    // End Product

    // Begin Sales Order
    Route::get('sales-order', [SalesOrderController::class, 'index']);
    Route::get('sales-order/view', [SalesOrderController::class, 'view']);
    Route::post('sales-order/save', [SalesOrderController::class, 'save']);
    Route::post('sales-order/update', [SalesOrderController::class, 'update']);
    Route::get('sales-order/delete-item', [SalesOrderController::class, 'deleteItem']);
    Route::get('sales-order/list-payment-method', [SalesOrderController::class, 'listPaymentMethod']);
    Route::get('sales-order/list-customer', [SalesOrderController::class, 'listCustomer']);
    // End Sales Order

    // Begin Purchase Order
    Route::get('purchase-order', [PurchaseOrderController::class, 'index']);
    Route::get('purchase-order/view', [PurchaseOrderController::class, 'view']);
    Route::post('purchase-order/save', [PurchaseOrderController::class, 'save']);
    Route::get('purchase-order/list-supplier', [PurchaseOrderController::class, 'listSupplier']);
    // End Purchase Order

    // Begin Expense
    Route::get('expense', [ExpenseController::class, 'index']);
    Route::get('expense/view', [ExpenseController::class, 'view']);
    Route::post('expense/save', [ExpenseController::class, 'save']);
    // End Expense

    // Begin Journal
    Route::get('journal', [JournalController::class, 'index']);
    // End Journal

    Route::post('auth/logout', [AuthController::class, 'logout']);
});

Route::post('auth/login', [AuthController::class, 'login']);
