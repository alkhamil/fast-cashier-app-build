<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrderPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sales_order_id');
            $table->bigInteger('payment_method_id');
            $table->bigInteger('amount')->default(0);
            $table->enum('is_paid', [0, 1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order_payments');
    }
}
