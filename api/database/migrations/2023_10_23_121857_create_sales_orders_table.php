<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('customer_id')->nullable();
            $table->string('sales_order_number');
            $table->enum('status', ["DRAFT", "PAID", "CANCELLED"])->default("DRAFT");
            $table->bigInteger('total_final_price')->default(0);
            $table->bigInteger('cash_amount')->default(0);
            $table->bigInteger('change_amount')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_orders');
    }
}
