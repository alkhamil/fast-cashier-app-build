<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class InitialDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // init data user
        $users = [
            [
                "email" => "kasir@pos.com",
                "fullname" => "Kasir",
                "username" => "kasir",
                "password" => Hash::make("kasir"),
                "role" => "KASIR"
            ],
            [
                "email"=> "admin@pos.com",
                "fullname" => "Admin",
                "username" => "admin",
                "password" => Hash::make("admin"),
                "role"=> "ADMIN"
            ]
        ];

        foreach ($users as $user) {
            DB::table('users')->insert([
                "email" => $user["email"],
                "fullname" => $user["fullname"],
                "username" => $user["username"],
                "password" => $user["password"],
                "role" => $user["role"],
                "created_at" => date('Y-m-d H:i:s')
            ]);
        }

        // init payment method
        $payment_methods = ["CASH", "TEMPO"];
        foreach ($payment_methods as $payment_method) {
            DB::table("payment_methods")->insert([
                "name" => $payment_method,
            ]);
        }

    }
}
