<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\Journal;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ExpenseController extends Controller
{
  public function index(Request $request)
  {
    try {

      $expenses = Expense::query();

      if (!is_null($request->expense_number)) {
        $expenses->where('expense_number', 'like', '%' . $request->expense_number . '%');
      }

      if (!is_null($request->description)) {
        $expenses->where('description', 'like', '%' . $request->description . '%');
      }

      if (!is_null($request->created_at)) {
        $expenses->whereDate('created_at', '=', $request->created_at);
      }

      if ($request->sort) {
        $order_type = 'asc';
        $order_column = $request->sort;
        if (str_contains($request->sort, '-')) {
          $order_type = 'desc';
          $order_column = substr($request->sort, 1);
        }

        $expenses->orderBy($order_column, $order_type);
      }

      $result = $expenses->latest('id')->paginate($request->per_page);

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function view(Request $request)
  {
    try {
      $expense = Expense::firstWhere('id', $request->id);

      if (!$expense) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $expense,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  private function saveJournal($item)
  {
    $journal = new Journal();
    $journal->journal_number = "JRN" . time();
    $journal->ref_number = $item["ref_number"];
    $journal->description = $item["description"];
    $journal->credit = $item["credit"];
    $journal->debit = $item["debit"];

    $latestJournal = DB::table('journals')
      ->latest('id')
      ->first();

    if ($latestJournal) {
      $journal->balance = $latestJournal->balance - $journal->debit;
    } else {
      $journal->balance = 0 - $journal->debit;
    }

    if (!$journal->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  public function save(Request $request)
  {
    DB::beginTransaction();
    try {
      $validator = Validator::make($request->all(), [
        'description' => 'required',
        'amount' => 'required',
      ]);


      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $expense = new Expense();
      $expense->expense_number = "EXS" . time();
      $expense->user_id = Auth::id();
      $expense->description = $request->description;
      $expense->amount = $request->amount;
      $expense->created_at = date('Y-m-d H:i:s');

      if (!$expense->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      $this->saveJournal([
        "ref_number" => $expense->expense_number,
        "description" => $expense->description,
        "credit" => 0,
        "debit" => $expense->amount
      ]);

      DB::commit();
      return response()->json([
        'data' => $expense,
        'message' => 'Successfuly Created!'
      ], 201);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

}
