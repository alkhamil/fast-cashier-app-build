<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {
        try {

            $users = User::query();

            if (!is_null($request->fullname)) {
                $users->where('fullname', 'like', '%' . $request->fullname . '%');
            }

            if (!is_null($request->email)) {
                $users->where('email', 'like', '%' . $request->email . '%');
            }

            if (!is_null($request->username)) {
                $users->where('username', 'like', '%' . $request->username . '%');
            }

            if (!is_null($request->is_active)) {
                $users->where('is_active', '=', $request->is_active);
            }

            $users->where('role', '=', 'KASIR');

            if ($request->sort) {
                $order_type = 'asc';
                $order_column = $request->sort;
                if (str_contains($request->sort, '-')) {
                    $order_type = 'desc';
                    $order_column = substr($request->sort, 1);
                }

                $users->orderBy($order_column, $order_type);
            }

            $result = $users->latest('id')->paginate($request->per_page);

            return response()->json([
                'data' => $result,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function view(Request $request)
    {
        try {
            $user = User::firstWhere('id', $request->id);

            if (!$user) {
                throw new Exception("Data not found!", 400);
            }

            return response()->json([
                'data' => $user,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'fullname' => 'required',
                'email' => 'required|unique:users,email,',
                'username' => 'required|unique:users,username,',
            ]);


            if ($validator->fails()) {
                throw new Exception($validator->errors(), 400);
            }

            $user = new User();
            $user->fullname = $request->fullname;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->created_at = date('Y-m-d H:i:s');

            if (!$user->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $user,
                'message' => 'Successfuly Created!'
            ], 201);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'fullname' => 'required'
            ]);

            if ($validator->fails()) {
                throw new Exception($validator->errors(), 400);
            }

            $user = User::firstWhere('id', $request->id);

            if (!$user) {
                throw new Exception("Data not found!", 400);
            }

            $user->fullname = $request->fullname;
            if (!is_null($user->password)) {
                $user->password = Hash::make($request->password);
            }
            $user->updated_at = date('Y-m-d H:i:s');

            if (!$user->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $user,
                'message' => 'Successfuly Updated!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {

            $user = User::firstWhere('id', $request->id);

            if (!$user) {
                throw new Exception("Data not found!", 400);
            }

            $user->is_active = "0";
            if (!$user->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $user,
                'message' => 'Successfuly Deleted!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function restore(Request $request)
    {
        DB::beginTransaction();
        try {

            $user = User::firstWhere('id', $request->id);

            if (!$user) {
                throw new Exception("Data not found!", 400);
            }

            $user->is_active = "1";
            if (!$user->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $user,
                'message' => 'Successfuly Restored!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
