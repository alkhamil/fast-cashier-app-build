<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Unit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UnitController extends Controller
{
    public function index(Request $request)
    {
        try {

            $units = Unit::query();

            if (!is_null($request->name)) {
                $units->where('name', 'like', '%' . $request->name . '%');
            }

            if (!is_null($request->description)) {
                $units->where('description', 'like', '%' . $request->description . '%');
            }

            if (!is_null($request->is_active)) {
                $units->where('is_active', '=', $request->is_active);
            }

            if ($request->sort) {
                $order_type = 'asc';
                $order_column = $request->sort;
                if (str_contains($request->sort, '-')) {
                    $order_type = 'desc';
                    $order_column = substr($request->sort, 1);
                }

                $units->orderBy($order_column, $order_type);
            }

            $result = $units->latest('id')->paginate($request->per_page);

            return response()->json([
                'data' => $result,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function view(Request $request)
    {
        try {
            $unit = Unit::firstWhere('id', $request->id);

            if (!$unit) {
                throw new Exception("Data not found!", 400);
            }

            return response()->json([
                'data' => $unit,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);


            if ($validator->fails()) {
                throw new Exception($validator->errors(), 400);
            }

            $unit = new Unit();
            $unit->name = $request->name;
            $unit->description = $request->description;
            $unit->created_at = date('Y-m-d H:i:s');

            if (!$unit->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $unit,
                'message' => 'Successfuly Created!'
            ], 201);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                throw new Exception($validator->errors(), 400);
            }

            $unit = Unit::firstWhere('id', $request->id);

            if (!$unit) {
                throw new Exception("Data not found!", 400);
            }

            $unit->name = $request->name;
            $unit->description = $request->description;
            $unit->updated_at = date('Y-m-d H:i:s');

            if (!$unit->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $unit,
                'message' => 'Successfuly Updated!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {

            $unit = Unit::firstWhere('id', $request->id);
            
            if (!$unit) {
                throw new Exception("Data not found!", 400);
            }
            
            $unit->is_active = "0";
            if (!$unit->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $unit,
                'message' => 'Successfuly Deleted!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function restore(Request $request)
    {
        DB::beginTransaction();
        try {

            $unit = Unit::firstWhere('id', $request->id);
            
            if (!$unit) {
                throw new Exception("Data not found!", 400);
            }
            
            $unit->is_active = "1";
            if (!$unit->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $unit,
                'message' => 'Successfuly Restored!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
