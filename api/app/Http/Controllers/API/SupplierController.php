<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Supplier;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SupplierController extends Controller
{
    public function index(Request $request)
    {
        try {

            $suppliers = Supplier::query();

            if (!is_null($request->name)) {
                $suppliers->where('name', 'like', '%' . $request->name . '%');
            }

            if (!is_null($request->address)) {
                $suppliers->where('address', 'like', '%' . $request->address . '%');
            }

            if (!is_null($request->phone)) {
                $suppliers->where('phone', 'like', '%' . $request->phone . '%');
            }

            if (!is_null($request->is_active)) {
                $suppliers->where('is_active', '=', $request->is_active);
            }

            if ($request->sort) {
                $order_type = 'asc';
                $order_column = $request->sort;
                if (str_contains($request->sort, '-')) {
                    $order_type = 'desc';
                    $order_column = substr($request->sort, 1);
                }

                $suppliers->orderBy($order_column, $order_type);
            }

            $result = $suppliers->latest('id')->paginate($request->per_page);

            return response()->json([
                'data' => $result,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function view(Request $request)
    {
        try {
            $supplier = Supplier::firstWhere('id', $request->id);

            if (!$supplier) {
                throw new Exception("Data not found!", 400);
            }

            return response()->json([
                'data' => $supplier,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);


            if ($validator->fails()) {
                throw new Exception($validator->errors(), 400);
            }

            $supplier = new Supplier();
            $supplier->name = $request->name;
            $supplier->address = $request->address;
            $supplier->phone = $request->phone;
            $supplier->created_at = date('Y-m-d H:i:s');

            if (!$supplier->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $supplier,
                'message' => 'Successfuly Created!'
            ], 201);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                throw new Exception($validator->errors(), 400);
            }

            $supplier = Supplier::firstWhere('id', $request->id);

            if (!$supplier) {
                throw new Exception("Data not found!", 400);
            }

            $supplier->name = $request->name;
            $supplier->address = $request->address;
            $supplier->phone = $request->phone;
            $supplier->updated_at = date('Y-m-d H:i:s');

            if (!$supplier->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $supplier,
                'message' => 'Successfuly Updated!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {

            $supplier = Supplier::firstWhere('id', $request->id);
            
            if (!$supplier) {
                throw new Exception("Data not found!", 400);
            }
            
            $supplier->is_active = "0";
            if (!$supplier->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $supplier,
                'message' => 'Successfuly Deleted!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function restore(Request $request)
    {
        DB::beginTransaction();
        try {

            $supplier = Supplier::firstWhere('id', $request->id);
            
            if (!$supplier) {
                throw new Exception("Data not found!", 400);
            }
            
            $supplier->is_active = "1";
            if (!$supplier->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $supplier,
                'message' => 'Successfuly Restored!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
