<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Journal;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\SalesOrder;
use App\Models\SalesOrderDetail;
use App\Models\SalesOrderPayment;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SalesOrderController extends Controller
{
  public function index(Request $request)
  {
    try {

      $sales_orders = SalesOrder::query()->with([
        'user',
        'customer',
        'sales_order_details.product',
        'sales_order_payments',
        'sales_order_payments.payment_method',
      ]);

      if (!is_null($request->sales_order_number)) {
        $sales_orders->where('sales_order_number', 'like', '%' . $request->sales_order_number . '%');
      }

      if (!is_null($request->user_fullname)) {
        $sales_orders->whereHas('user', function ($query) use ($request) {
          return $query->where('fullname', 'like', '%' . $request->user_fullname . '%');
        });
      }

      if (!is_null($request->customer_name)) {
        $sales_orders->whereHas('customer', function ($query) use ($request) {
          return $query->where('name', 'like', '%' . $request->customer_name . '%');
        });
      }

      if (!is_null($request->status)) {
        $sales_orders->where('status', '=', $request->status);
      }

      if (!is_null($request->created_at)) {
        $sales_orders->whereDate('created_at', '=', $request->created_at);
      }

      if ($request->sort) {
        $order_type = 'asc';
        $order_column = $request->sort;
        if (str_contains($request->sort, '-')) {
          $order_type = 'desc';
          $order_column = substr($request->sort, 1);
        }

        $sales_orders->orderBy($order_column, $order_type);
      }

      $result = $sales_orders->latest('created_at')->paginate($request->per_page);

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function view(Request $request)
  {
    try {
      $sales_order = SalesOrder::query()->with([
        'user',
        'customer',
        'sales_order_details',
        'sales_order_details.product',
        'sales_order_payments',
        'sales_order_payments.payment_method',
      ])->where('id', '=', $request->id)->first();

      if (!$sales_order) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $sales_order,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  private function saveJournal($item)
  {
    $journal = new Journal();
    $journal->journal_number = "JRN" . time();
    $journal->ref_number = $item["ref_number"];
    $journal->description = $item["description"];
    $journal->credit = $item["credit"];
    $journal->debit = $item["debit"];

    $latestJournal = DB::table('journals')
      ->latest('id')
      ->first();

    if ($latestJournal) {
      $journal->balance = $latestJournal->balance + $journal->credit;
    } else {
      $journal->balance = 0 + $journal->credit;
    }

    if (!$journal->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  private function saveProductStock(Product $product, $qty, $description)
  {
    $product_stock = new ProductStock();

    $product_stock->product_id = $product->id;
    $product_stock->user_id = Auth::id();
    $product_stock->description = $description;
    $product_stock->type = "OUT";

    $latestProductStock = DB::table('product_stocks')
      ->where('product_id', $product->id)
      ->latest('id')
      ->first();

    if ($latestProductStock) {
      $product_stock->amount = $latestProductStock->amount - $qty;
    } else {
      $product_stock->amount = 0 - $qty;
    }

    if (!$product_stock->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }

    $product->stock = $product_stock->amount;

    if (!$product->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  public function save(Request $request)
  {
    DB::beginTransaction();
    try {
      $validator = Validator::make($request->all(), [
        'total_final_price' => 'required',
        'cash_amount' => 'required',
        'change_amount' => 'required',
        'sales_order_details' => 'required|array',
        'sales_order_details.*.product_id' => 'required|exists:products,id',
        'sales_order_details.*.qty' => 'required',
        'sales_order_details.*.price' => 'required',
        'sales_order_details.*.final_price' => 'required',
      ]);


      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $sales_order = new SalesOrder();
      $sales_order->user_id = Auth::id();
      $sales_order->customer_id = $request->customer_id;
      $sales_order->sales_order_number = "SLO" . time();
      $sales_order->total_final_price = $request->total_final_price;
      $sales_order->cash_amount = $request->cash_amount;
      $sales_order->change_amount = $request->change_amount;
      $sales_order->created_at = date('Y-m-d H:i:s');

      if ($sales_order->cash_amount >= $sales_order->total_final_price) {
        $sales_order->status = "PAID";
      }

      if (!$sales_order->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      if (!empty($request->sales_order_details) && is_array($request->sales_order_details)) {
        foreach ($request->sales_order_details as $sales_order_detail) {
          $_sales_order_detail = new SalesOrderDetail();
          $_sales_order_detail->sales_order_id = $sales_order->id;
          $_sales_order_detail->product_id = $sales_order_detail["product_id"];
          $_sales_order_detail->qty = $sales_order_detail["qty"];
          $_sales_order_detail->price = $sales_order_detail["price"];
          $_sales_order_detail->final_price = $sales_order_detail["final_price"];

          $product = Product::firstWhere("id", $_sales_order_detail->product_id);
          if ($product->stock < $_sales_order_detail->qty) {
            throw new Exception('
              Stock product tidak cukup. sisa stock untuk product ' . $product->name . ' [' . $product->stock . ']',
              500
            );
          }

          if ($product && $sales_order->status === "PAID") {
            $this->saveProductStock($product, $_sales_order_detail->qty, "Penjualan");
          }

          if (!$_sales_order_detail->save()) {
            throw new Exception('Failed transaction DB!', 500);
          }

        }
      }

      if (!empty($request->sales_order_payments) && is_array($request->sales_order_payments)) {
        foreach ($request->sales_order_payments as $sales_order_payment) {
          $_sales_order_payment = new SalesOrderPayment();
          $_sales_order_payment->sales_order_id = $sales_order->id;
          $_sales_order_payment->payment_method_id = $sales_order_payment["payment_method_id"];
          $_sales_order_payment->amount = $sales_order_payment["amount"];

          if (!$_sales_order_payment->save()) {
            throw new Exception('Failed transaction DB!', 500);
          }

        }
      }

      if ($sales_order->status === "PAID") {
        $this->saveJournal([
          "ref_number" => $sales_order->sales_order_number,
          "description" => "Penjualan",
          "credit" => $sales_order->total_final_price,
          "debit" => 0
        ]);
      }

      DB::commit();
      return response()->json([
        'data' => $sales_order,
        'message' => 'Successfuly Created!'
      ], 201);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
  public function update(Request $request)
  {
    DB::beginTransaction();
    try {
      $validator = Validator::make($request->all(), [
        'total_final_price' => 'required',
        'cash_amount' => 'required',
        'change_amount' => 'required',
        'sales_order_details.*.product_id' => 'required|exists:products,id',
        'sales_order_details.*.qty' => 'required',
        'sales_order_details.*.price' => 'required',
        'sales_order_details.*.final_price' => 'required',
      ]);


      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $sales_order = SalesOrder::firstWhere('id', $request->id);

      if (!$sales_order) {
        throw new Exception("Data not found!", 400);
      }

      $sales_order->user_id = Auth::id();
      $sales_order->customer_id = $request->customer_id;
      $sales_order->total_final_price = $request->total_final_price;
      $sales_order->cash_amount = $request->cash_amount;
      $sales_order->change_amount = $request->change_amount;
      $sales_order->updated_at = date('Y-m-d H:i:s');

      if ($sales_order->cash_amount >= $sales_order->total_final_price && $sales_order->total_final_price > 0) {
        $sales_order->status = "PAID";
      }

      if (!$sales_order->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      if (!empty($request->sales_order_details) && is_array($request->sales_order_details)) {
        foreach ($request->sales_order_details as $sales_order_detail) {
          if (!empty($sales_order_detail["id"])) {
            $_sales_order_detail = SalesOrderDetail::firstWhere('id', $sales_order_detail["id"]);
          } else {
            $_sales_order_detail = new SalesOrderDetail();
          }
          $_sales_order_detail->sales_order_id = $sales_order->id;
          $_sales_order_detail->product_id = $sales_order_detail["product_id"];
          $_sales_order_detail->qty = $sales_order_detail["qty"];
          $_sales_order_detail->price = $sales_order_detail["price"];
          $_sales_order_detail->final_price = $sales_order_detail["final_price"];

          $product = Product::firstWhere("id", $_sales_order_detail->product_id);

          if ($product->stock < $_sales_order_detail->qty) {
            throw new Exception('
              Stock product tidak cukup. sisa stock untuk product ' . $product->name . ' [' . $product->stock . ']',
              500
            );
          }
          
          if ($product && $sales_order->status === "PAID") {
            $this->saveProductStock($product, $_sales_order_detail->qty, "Penjualan");
          }

          if (!$_sales_order_detail->save()) {
            throw new Exception('Failed transaction DB!', 500);
          }

        }
      }

      if (!empty($request->sales_order_payments) && is_array($request->sales_order_payments)) {
        foreach ($request->sales_order_payments as $sales_order_payment) {
          if (!empty($sales_order_payment["id"])) {
            $_sales_order_payment = SalesOrderPayment::firstWhere('id', $sales_order_payment["id"]);
          } else {
            $_sales_order_payment = new SalesOrderPayment();
          }
          $_sales_order_payment->sales_order_id = $sales_order->id;
          $_sales_order_payment->payment_method_id = $sales_order_payment["payment_method_id"];
          $_sales_order_payment->amount = $sales_order_payment["amount"];

          if (!$_sales_order_payment->save()) {
            throw new Exception('Failed transaction DB!', 500);
          }

        }
      }

      if ($sales_order->status === "PAID") {
        $this->saveJournal([
          "ref_number" => $sales_order->sales_order_number,
          "description" => "Penjualan",
          "credit" => $sales_order->total_final_price,
          "debit" => 0
        ]);
      }

      DB::commit();
      return response()->json([
        'data' => $sales_order,
        'message' => 'Successfuly Created!'
      ], 201);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function deleteItem(Request $request)
  {
    DB::beginTransaction();
    try {

      $sales_order_detail = SalesOrderDetail::firstWhere('id', $request->id);

      if (!$sales_order_detail) {
        throw new Exception("Data not found!", 400);
      }

      if (!$sales_order_detail->delete()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      DB::commit();
      return response()->json([
        'data' => $sales_order_detail,
        'message' => 'Successfuly Deleted!'
      ], 200);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function listPaymentMethod()
  {
    try {
      $payment_methods = PaymentMethod::get();

      if (!$payment_methods) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $payment_methods,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
  public function listCustomer()
  {
    try {
      $customers = Customer::get();

      if (!$customers) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $customers,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

}
