<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Journal;
use Exception;
use Illuminate\Http\Request;

class JournalController extends Controller
{
  public function index(Request $request)
  {
    try {

      $journals = Journal::query();

      if (!is_null($request->journal_number)) {
        $journals->where('journal_number', 'like', '%' . $request->journal_number . '%');
      }

      if (!is_null($request->ref_number)) {
        $journals->where('ref_number', 'like', '%' . $request->ref_number . '%');
      }

      if (!is_null($request->description)) {
        $journals->where('description', 'like', '%' . $request->description . '%');
      }

      if (!is_null($request->created_at)) {
        $journals->whereDate('created_at', '=', $request->created_at);
      }

      if ($request->sort) {
        $order_type = 'asc';
        $order_column = $request->sort;
        if (str_contains($request->sort, '-')) {
          $order_type = 'desc';
          $order_column = substr($request->sort, 1);
        }

        $journals->orderBy($order_column, $order_type);
      }

      $result = $journals->latest('created_at')->paginate($request->per_page);

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
}
