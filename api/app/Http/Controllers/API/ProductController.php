<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductStock;
use App\Models\Unit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        try {

            $products = Product::query()->with(['category', 'unit']);

            if (!is_null($request->name)) {
                $products->where('name', 'like', '%' . $request->name . '%');
            }

            if (!is_null($request->barcode)) {
                $products->where('barcode', 'like', '%' . $request->barcode . '%');
            }

            if (!is_null($request->catagory_name)) {
                $products->whereHas('category', function($query) use ($request) {
                    return $query->where('name','like', '%' . $request->catagory_name . '%');
                });
            }

            if (!is_null($request->category_id)) {
                $products->where('category_id', '=', $request->category_id);   
            }

            if (!is_null($request->unit_name)) {
                $products->whereHas('unit', function($query) use ($request) {
                    return $query->where('name','like', '%' . $request->unit_name . '%');
                });
            }

            if (!is_null($request->is_active)) {
                $products->where('is_active', '=', $request->is_active);
            }

            if ($request->sort) {
                $order_type = 'asc';
                $order_column = $request->sort;
                if (str_contains($request->sort, '-')) {
                    $order_type = 'desc';
                    $order_column = substr($request->sort, 1);
                }

                $products->orderBy($order_column, $order_type);
            }

            $result = $products->latest('id')->paginate($request->per_page);

            return response()->json([
                'data' => $result,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function view(Request $request)
    {
        try {
            $product = Product::firstWhere('id', $request->id);

            if (!$product) {
                throw new Exception("Data not found!", 400);
            }

            return response()->json([
                'data' => $product,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    private function saveProductPrice(Product $product)
    {
        $product_price = new ProductPrice();

        $product_price->product_id = $product->id;
        $product_price->user_id = Auth::id();
        $product_price->base_price = $product->base_price;
        $product_price->sell_price = $product->sell_price;

        if (!$product_price->save()) {
            throw new Exception('Failed transaction DB!', 500);
        }
    }

    private function saveProductStock(Product $product, $description)
    {
        $product_stock = new ProductStock();

        $product_stock->product_id = $product->id;
        $product_stock->user_id = Auth::id();
        $product_stock->amount = $product->stock;
        $product_stock->description = $description;
        $product_stock->type = "IN";

        if (!$product_stock->save()) {
            throw new Exception('Failed transaction DB!', 500);
        }
    }

    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'category_id' => 'required|exists:categories,id',
                'unit_id' => 'required|exists:units,id',
                'name' => 'required',
                'barcode' => 'required',
                'base_price' => 'required',
                'sell_price' => 'required',
                'stock' => 'required',
            ]);


            if ($validator->fails()) {
                throw new Exception($validator->errors(), 400);
            }

            $product = new Product();
            $product->category_id = $request->category_id;
            $product->unit_id = $request->unit_id;
            $product->name = $request->name;
            $product->barcode = $request->barcode;
            $product->base_price = $request->base_price;
            $product->sell_price = $request->sell_price;
            $product->stock = $request->stock;
            $product->created_at = date('Y-m-d H:i:s');

            if (!$product->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            $this->saveProductPrice($product);
            $this->saveProductStock($product, "Save Product");

            DB::commit();
            return response()->json([
                'data' => $product,
                'message' => 'Successfuly Created!'
            ], 201);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'category_id' => 'required',
                'unit_id' => 'required',
                'name' => 'required',
                'barcode' => 'required',
                'base_price' => 'required',
                'sell_price' => 'required',
                'stock' => 'required',
            ]);

            if ($validator->fails()) {
                throw new Exception($validator->errors(), 400);
            }

            $product = Product::firstWhere('id', $request->id);

            if (!$product) {
                throw new Exception("Data not found!", 400);
            }

            $product->category_id = $request->category_id;
            $product->unit_id = $request->unit_id;
            $product->name = $request->name;
            $product->barcode = $request->barcode;
            $product->base_price = $request->base_price;
            $product->sell_price = $request->sell_price;
            $product->stock = $request->stock;
            $product->updated_at = date('Y-m-d H:i:s');

            if (!$product->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            $this->saveProductPrice($product);
            $this->saveProductStock($product, "Update Product");

            DB::commit();
            return response()->json([
                'data' => $product,
                'message' => 'Successfuly Updated!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {

            $product = Product::firstWhere('id', $request->id);
            
            if (!$product) {
                throw new Exception("Data not found!", 400);
            }
            
            $product->is_active = "0";
            if (!$product->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $product,
                'message' => 'Successfuly Deleted!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function restore(Request $request)
    {
        DB::beginTransaction();
        try {

            $product = Product::firstWhere('id', $request->id);
            
            if (!$product) {
                throw new Exception("Data not found!", 400);
            }
            
            $product->is_active = "1";
            if (!$product->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $product,
                'message' => 'Successfuly Restored!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function listCategory()
    {
        try {
            $categories = Category::get();

            if (!$categories) {
                throw new Exception("Data not found!", 400);
            }

            return response()->json([
                'data' => $categories,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function listUnit()
    {
        try {
            $units = Unit::get();

            if (!$units) {
                throw new Exception("Data not found!", 400);
            }

            return response()->json([
                'data' => $units,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

}
