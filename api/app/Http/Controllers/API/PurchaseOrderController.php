<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Journal;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderDetail;
use App\Models\Supplier;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PurchaseOrderController extends Controller
{
  public function index(Request $request)
  {
    try {

      $purchase_orders = PurchaseOrder::query()->with([
        'user',
        'supplier',
        'purchase_order_details',
        'purchase_order_details.product'
      ]);

      if (!is_null($request->purchase_order_number)) {
        $purchase_orders->where('purchase_order_number', 'like', '%' . $request->purchase_order_number . '%');
      }

      if (!is_null($request->user_fullname)) {
        $purchase_orders->whereHas('user', function ($query) use ($request) {
          return $query->where('fullname', 'like', '%' . $request->user_fullname . '%');
        });
      }

      if (!is_null($request->supplier_name)) {
        $purchase_orders->whereHas('supplier', function ($query) use ($request) {
          return $query->where('name', 'like', '%' . $request->supplier_name . '%');
        });
      }

      if (!is_null($request->created_at)) {
        $purchase_orders->whereDate('created_at', '=', $request->created_at);
      }

      if ($request->sort) {
        $order_type = 'asc';
        $order_column = $request->sort;
        if (str_contains($request->sort, '-')) {
          $order_type = 'desc';
          $order_column = substr($request->sort, 1);
        }

        $purchase_orders->orderBy($order_column, $order_type);
      }

      $result = $purchase_orders->latest('id')->paginate($request->per_page);

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function view(Request $request)
  {
    try {
      $purchase_order = PurchaseOrder::query()->with([
        'user',
        'supplier',
        'purchase_order_details',
        'purchase_order_details.product'
      ])->where('id', '=', $request->id)->first();

      if (!$purchase_order) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $purchase_order,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  private function saveJournal($item)
  {
    $journal = new Journal();
    $journal->journal_number = "JRN" . time();
    $journal->ref_number = $item["ref_number"];
    $journal->description = $item["description"];
    $journal->credit = $item["credit"];
    $journal->debit = $item["debit"];

    $latestJournal = DB::table('journals')
      ->latest('id')
      ->first();

    if ($latestJournal) {
      $journal->balance = $latestJournal->balance - $journal->debit;
    } else {
      $journal->balance = 0 - $journal->debit;
    }

    if (!$journal->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  private function saveProductStock(Product $product, $qty, $description)
  {
    $product_stock = new ProductStock();

    $product_stock->product_id = $product->id;
    $product_stock->user_id = Auth::id();
    $product_stock->description = $description;
    $product_stock->type = "IN";

    $latestProductStock = DB::table('product_stocks')
      ->where('product_id', $product->id)
      ->latest('id')
      ->first();

    if ($latestProductStock) {
      $product_stock->amount = $latestProductStock->amount + $qty;
    } else {
      $product_stock->amount = 0 + $qty;
    }

    if (!$product_stock->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }

    $product->stock = $product_stock->amount;

    if (!$product->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  public function save(Request $request)
  {
    DB::beginTransaction();
    try {
      $validator = Validator::make($request->all(), [
        'supplier_id' => 'required',
        'total_final_price' => 'required',
        'purchase_order_details' => 'required|array',
        'purchase_order_details.*.product_id' => 'required|exists:products,id',
        'purchase_order_details.*.qty' => 'required',
        'purchase_order_details.*.price' => 'required',
        'purchase_order_details.*.final_price' => 'required',
      ]);


      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $purchase_order = new PurchaseOrder();
      $purchase_order->user_id = Auth::id();
      $purchase_order->supplier_id = $request->supplier_id;
      $purchase_order->purchase_order_number = "PRO" . time();
      $purchase_order->total_final_price = $request->total_final_price;
      $purchase_order->created_at = date('Y-m-d H:i:s');

      if (!$purchase_order->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      if (!empty($request->purchase_order_details) && is_array($request->purchase_order_details)) {
        foreach ($request->purchase_order_details as $sales_order_detail) {
          $_purchase_order_detail = new PurchaseOrderDetail();
          $_purchase_order_detail->purchase_order_id = $purchase_order->id;
          $_purchase_order_detail->product_id = $sales_order_detail["product_id"];
          $_purchase_order_detail->qty = $sales_order_detail["qty"];
          $_purchase_order_detail->price = $sales_order_detail["price"];
          $_purchase_order_detail->final_price = $sales_order_detail["final_price"];

          $product = Product::firstWhere("id", $_purchase_order_detail->product_id);
          if ($product) {
            $this->saveProductStock($product, $_purchase_order_detail->qty, "Pembelian");
          }

          if (!$_purchase_order_detail->save()) {
            throw new Exception('Failed transaction DB!', 500);
          }

        }
      }

      $this->saveJournal([
        "ref_number" => $purchase_order->purchase_order_number,
        "description" => "Pembelian",
        "credit" => 0,
        "debit" => $purchase_order->total_final_price
      ]);

      DB::commit();
      return response()->json([
        'data' => $purchase_order,
        'message' => 'Successfuly Created!'
      ], 201);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
  public function listSupplier()
  {
    try {
      $suppliers = Supplier::get();

      if (!$suppliers) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $suppliers,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
}
