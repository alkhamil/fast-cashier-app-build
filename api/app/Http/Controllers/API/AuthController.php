<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            $request->validate([
                'username' => 'required',
                'password' => 'required',
            ]);


            $user = User::where('username', $request->username)
                ->first();

            if (!$user) {
                throw new Exception("Authentication Failed", 401);
            }

            $credentials['email'] = $user->email;
            $credentials['password'] = $request->password;

            if (!Auth::attempt($credentials)) {
                throw new Exception("Authentication Failed", 401);
            }

            if (!Hash::check($request->password, $user->password, [])) {
                throw new Exception("Email or password doesn't match", 400);
            }

            $token = $user->createToken('authToken')->plainTextToken;

            return response()->json([
                'message' => 'Login Successfully',
                'data' => [
                    'token' => $token,
                    'user' => $user
                ]
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->tokens()->delete();

        return response()->json([
            'message' => 'Token revoked',
            'data' => $user
        ], 200);
    }

}
